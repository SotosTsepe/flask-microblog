from flask import abort, make_response, request, url_for

from app import db
from app.api import api_bp
from app.api.auth import token_auth
from app.models import User
from app.api.errors import bad_request


@api_bp.route('/users')
def get_users():
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    return User.to_collection_dict(User.query, page, per_page, endpoint='api.get_users')


@api_bp.route('/users', methods=('POST',))
def create_user():
    data = request.get_json() or {}
    if 'username' not in data or 'email' not in data or 'password' not in data:
        return bad_request('username, email and password fields must be included')
    if User.query.filter_by(username=data['username']).first():
        return bad_request('User already exists with the specified username.')
    if User.query.filter_by(email=data['email']).first():
        return bad_request('User already exists with the specified email.')
    user = User()
    user.from_dict(data, new_user=True)
    db.session.add(user)
    db.session.commit()
    return make_response(
        user.to_dict(),
        201,
        {'Location': url_for('api.get_user', id=user.id)}
    )


@api_bp.route('/users/<int:id>')
@token_auth.login_required
def get_user(id):
    return User.query.get_or_404(id).to_dict()


@api_bp.route('/users/<int:id>', methods=('PUT',))
@token_auth.login_required
def update_user(id):
    user = User.query.get_or_404(id)
    data = request.get_json() or {}
    if not data:
        abort(415)
    if token_auth.current_user().id != id:
        abort(403)
    if 'username' in data: 
        if data['username'] == user.username:
            return bad_request('Username does not differ from the previous one.')
        if User.query.filter_by(username=data['username']).first():
            return bad_request('User already exists with the specified username.')
    if 'email' in data: 
        if data['email'] == user.email:
            return bad_request('Email address does not differ from the previous one.')
        if User.query.filter_by(email=data['email']).first():
            return bad_request('User already exists with the specified email address.')
    user.from_dict(data)
    db.session.commit()
    return user.to_dict()


@api_bp.route('/users/<int:id>/followers')
@token_auth.login_required
def get_followers(id):
    user = User.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    return User.to_collection_dict(
        user.followers,
        page,
        per_page,
        endpoint='api.get_followers',
        id=id
    )


@api_bp.route('/users/<int:id>/following')
@token_auth.login_required
def get_following(id):
    user = User.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    return User.to_collection_dict(
        user.followed,
        page,
        per_page,
        endpoint='api.get_following',
        id=id
    )
